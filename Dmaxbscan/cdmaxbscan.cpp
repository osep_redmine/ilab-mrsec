/*
 * cdmaxbscan.cpp
 *
 *  Created on: Oct 30, 2012
 *      Author: Tharanga Mahakumarage
 */

#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <iomanip>
#include "cdmaxbscan.h"

using namespace std;

DmaxbScan::DmaxbScan() {
	_cserial = new CSerial();

	// initialize the variables
	// allocate memory for read and write buffers
	_readbuff = new char[DMAXB_MAX_READ_BUFFSIZE];
	_writebuff = new char[DMAXB_MAX_WRITE_BUFFSIZE];

//	memset( _readbuff, 0, DMAXB_MAX_READ_BUFFSIZE );
//	memset( _writebuff, 0, DMAXB_MAX_WRITE_BUFFSIZE);
	_readbuff[0] = '\0';
	_writebuff[0] = '\0';
	_maxReadSize = DMAXB_MAX_READ_BUFFSIZE;
	_maxWriteSize = DMAXB_MAX_WRITE_BUFFSIZE;

}

DmaxbScan::~DmaxbScan() {
	delete[] _readbuff;
	delete[] _writebuff;
	delete _cserial;
}

DmaxbScan& DmaxbScan::initialize(int comPort, int baudRate ) {
	// Create the serial port connection
	_cserial->Open(comPort, baudRate);
	return *this;
}

string DmaxbScan::_prepareSendDataString(){
	ostringstream sstream;
	string tempStr;

	sstream << "\x02" << "M1" << _lrsetting << "," << _scan << "," << _modetwo
			<< "," << _modeThree << "," << _axis << "," << _startAngle << ","
			<< _endAngle << "," << fixed << setprecision(3) << _scanSpeed << ","
			<< _sampleInterval << "," << setprecision(2) << _ftTime << ","
			<< _fullScale << "," << _cpscount <<","<< "0,100,1,0,10,0,0,TEST.TXT\x0D\x0A";

	tempStr = sstream.str();
	return tempStr;
}

DmaxbScan& DmaxbScan::prepareSendDataHex() {
	ostringstream result;
	string tempStr;
	unsigned int i;

	tempStr = _prepareSendDataString();

	for (i = 0; i < tempStr.length(); i++) {
		result << setw(2) << setfill('0') << hex << uppercase;
		result << static_cast<unsigned int>(tempStr.at(i));
	}

	strcpy(_writebuff, result.str().c_str());
	return *this;
}

DmaxbScan& DmaxbScan::prepareSendData() {
	string tempStr;

	tempStr = _prepareSendDataString();
	strcpy(_writebuff, tempStr.c_str());
	return *this;
}

DmaxbScan& DmaxbScan::writeToSerial() {
	int length = strlen(_writebuff);
	cout << length << endl;
	_cserial->SendData((const char*) _writebuff, length);
	return *this;
}

DmaxbScan& DmaxbScan::readFromSerial() {
	int time = 12500;	// we wait 25 seconds to receive data.
	int result;

	cout << "Reading from serial port";

	while (time > 0) {
		result = _cserial->ReadDataWaiting();

		if (result) {
			break;
		}
		cout << ".";
		time -= 500;
		Sleep(500);
	}

	cout << endl;

	result = _cserial->ReadData((void*) _readbuff, _maxReadSize);
	if (result < _maxReadSize) {
		_readbuff[result] = '\0';
	}

	return *this;
}

DmaxbScan& DmaxbScan::analyzeData() {
	return *this;
}

DmaxbScan& DmaxbScan::print() {
	cout << "Following data were read from the serial";
	cout << this->getReadbuff() << endl;
	return *this;
}

void DmaxbScan::close() {
	cout << "Closing the serial connection" << endl;
	_cserial->Close();
}

