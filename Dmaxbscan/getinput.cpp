/*
 * getinput.cpp
 *
 *  Created on: Oct 30, 2012
 *      Author: Tharanga Mahakumarage
 */
#include <iostream>
#include <iomanip>
#include <limits>
#include <cmath>
#include "getinput.h"

using namespace std;


inline void _getInt(int& var, int min, int max, string message){
	for(;;){

		cout << message ;
		if( cin  >> var ){
			if( var >= min && var <= max ){
				break;
			}
		}

		cin.clear();
		cin.ignore( numeric_limits<streamsize>::max(), '\n');
		cout << "Invalid choice entered, please re-enter        " << endl;
	}
}

inline void _getLong(long& var, long min, long max, string message){
	for(;;){

		cout << message ;
		if( cin  >> var ){
			if( var >= min && var <= max ){
				break;
			}
		}

		cin.clear();
		cin.ignore( numeric_limits<streamsize>::max(), '\n');
		cout << "Invalid choice entered, please re-enter        " << endl;
	}
}

inline void _getFloat(float& var, float min, float max, string message){
	for(;;){

		cout << message ;
		if( cin  >> var ){
			if( var >= min && var <= max ){
				break;
			}
		}

		cin.clear();
		cin.ignore( numeric_limits<streamsize>::max(), '\n');
		cout << "Invalid choice entered, please re-enter        " << endl;
	}
}

void _getScanUnit(int& lrSetting){
	string message = "Left or Right  ( 1. Left, 2. Right )                             : ";
	_getInt( lrSetting, 1, 2, message );
}

void _getScanMode(int& scanMode){
	string message = "Mode  ( 1. Continuous, 2. Step )                                 : ";
	_getInt( scanMode, 1, 2, message );
}

void _getScanModeTwo(int& scanModeTwo){
	string message = "Mode2 ( 1. Standard, 2. Cumulative, 3. Skip )                    : ";
	_getInt( scanModeTwo, 1, 3, message );
}

void _getScanModeThree(int& scanModeThree){
	string message = "Mode3 ( 0 - 10000 )                                              : ";
	_getInt( scanModeThree, 0, 10000, message );
}

void _getAxis( int& axis ){
	string message = "Axis  ( 1. 2theta/theta, 2. 2theta, 3. Theta )                   : ";
	_getInt( axis, 1, 3, message );
}

void _getStartAngle( int& startAngle ){
	string message = "Start Angle  ( unit 1 degree )                                   : ";
	_getInt( startAngle, 0, 360, message );
}

void _getEndAngle( int& endAngle ){
	string message = "End Angle    ( unit 1 degree )                                   : ";
	_getInt( endAngle, 0, 360, message );
}

void _getScanSpeed( float& scanSpeed ){
	string message = "Scan Speed   ( 0.002 deg. to 90 deg./min in 0.002 steps )        : ";
	_getFloat(scanSpeed, 0.002, 90, message );

	scanSpeed = ((int)(scanSpeed/0.002))*0.002;
}

void _getSampleInterval( float& sampleInterval ){
	string message = "Sampling Interval( 0.002 deg. to 120 deg. in 0.002 steps )       : ";
	_getFloat(sampleInterval, 0.002, 120, message );
	sampleInterval = ((int)(sampleInterval/0.002))*0.002;
}

void _getFtTime( float& ftTime ){
	string message = "FT Time      ( 0.01 sec. to 600 sec. in 0.01 steps )             : ";
	_getFloat( ftTime, 0.01, 600, message );
	ftTime = ((int)(ftTime/0.01))*0.01;
}

void _getFullScale( long& fullScale ){
	string message = "Fullscale    ( 10^2 to 10^6 in 100 steps)                        : ";
	_getLong( fullScale, 100, 1000000, message );

	// if not a 100 step round down.
	if( fullScale % 100 != 0 ){
		fullScale = ( fullScale / 100 ) * 100;
		cout << "Input rounded down to "<< fullScale << endl;
	}
}

void _getCpsCount( int& cpsCount ){
	string message = "CPS/COUNT ( 1. CPS Linear, 2. Count Linear, 3. CPS Log, 4. Count Log ) : ";
	_getInt( cpsCount, 1, 4, message );
}


void getUserInput( DmaxbScan &scan) {
	int lrSetting;
	int scanMode;
	int scanModeTwo;
	int scanModeThree;
	int axis;
	int startAngle;
	int endAngle;
	float scanSpeed;
	float sampleInterval;
	float ftTime;
	long fullScale;
	int cpsCount;

	cout << "-----------------------------------------------------------------" << endl;
	cout << "              DMAX/B Scan                                        " << endl;
	cout << "-----------------------------------------------------------------" << endl << endl;
	cout << "Please insert the inputs for the scan                            " << endl;
	_getScanUnit( lrSetting );
	_getScanMode( scanMode );
	_getScanModeTwo( scanModeTwo );
	_getScanModeThree( scanModeThree );
	_getAxis( axis );
	_getStartAngle( startAngle );
	_getEndAngle( endAngle );
	_getScanSpeed( scanSpeed );
	_getSampleInterval( sampleInterval );
	_getFtTime( ftTime );
	//_getFullScale( fullScale );
	//_getCpsCount( cpsCount );

	// insert the collected variable to the dmaxbscan object.
	scan.setLrsetting((DMAXB_LRSetting) lrSetting )
		.setScan((DMAXB_Scan) scanMode)
		.setModetwo((DMAXB_ModeTwo) scanModeTwo)
		.setModeThree( scanModeThree )
		.setAxis((DMAXB_Axis) axis )
		.setStartAngle( startAngle )
		.setEndAngle( endAngle )
		.setScanSpeed( scanSpeed )
		.setSampleInterval( sampleInterval )
		.setFtTime( ftTime )
		// .setFullScale( fullScale )
		// .setCpscount( (DMAXB_CpsCount) cpsCount );
		.setFullScale( 100 )
		.setCpscount( CPS_LINEAR );
        
        UNREFERENCED_PARAMETER( fullScale );
        UNREFERENCED_PARAMETER( cpsCount );
}




