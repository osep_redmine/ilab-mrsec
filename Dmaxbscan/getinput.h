/*
 * getinput.h
 *
 *  Created on: Oct 30, 2012
 *      Author: Tharumax
 */

#ifndef GETINPUT_H_
#define GETINPUT_H_

#include "cdmaxbscan.h"

void getUserInput( DmaxbScan &scan );

#endif /* GETINPUT_H_ */
