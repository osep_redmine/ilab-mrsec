/*
 * main.cpp
 *
 *  Created on: Oct 29, 2012
 *      Author: Tharanga Mahakuarage
 */
#include <iostream>
#include "cdmaxbscan.h"
#include "getinput.h"

using namespace std;

int main(int argc, char* argv[]){
	DmaxbScan dmaxbscan;
	bool hexFlag = FALSE;

	if( argc == 2 && !strcmp( argv[1], "--hex") ){
		hexFlag = TRUE;
	}

	dmaxbscan.initialize( 3, 9600 );
	getUserInput( dmaxbscan );

	// prepare the data in DMAXB format for measure command.

	if( hexFlag ){
		dmaxbscan.prepareSendDataHex();
	}else{
		dmaxbscan.prepareSendData();
	}

	cout <<"Writing following data to serial port" << endl<< dmaxbscan.getWritebuff() << endl;
	dmaxbscan.writeToSerial();

	// get the incoming data to serial port
	dmaxbscan.readFromSerial();
	cout << "Following data was read from the serial port" << endl << dmaxbscan.getReadbuff() << endl;

	// close the serial and discard the dmaxscan object
	dmaxbscan.close();

	return 0;
}

