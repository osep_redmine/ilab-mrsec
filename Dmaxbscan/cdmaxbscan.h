/*
 * cdmaxbscan.h
 *
 *  Created on: Nov 1, 2012
 *      Author: Tharanga Mahakumarage
 */

#ifndef CDMAXBSCAN_H_
#define CDMAXBSCAN_H_

#include <iostream>
#include "Serial.h"
#include "settings.h"

#define DMAXB_MAX_READ_BUFFSIZE  256
#define DMAXB_MAX_WRITE_BUFFSIZE 256

class DmaxbScan {
private:
	CSerial* _cserial;
	char* _readbuff;
	char* _writebuff;
	DMAXB_LRSetting _lrsetting;
	DMAXB_Axis _axis;
	DMAXB_Scan _scan;
	DMAXB_Unit _unit;
	DMAXB_ModeTwo _modetwo;
	DMAXB_CpsCount _cpscount;
	int _modeThree;
	int _startAngle;
	int _endAngle;
	int _maxReadSize;
	int _maxWriteSize;
	float _scanSpeed;
	float _sampleInterval;
	float _ftTime;
	long _fullScale;

public:
	DmaxbScan();
	virtual ~DmaxbScan();

	DMAXB_Axis getAxis() const {
		return _axis;
	}

	DmaxbScan& setAxis(DMAXB_Axis axis) {
		_axis = axis;
		return *this;
	}

	DMAXB_CpsCount getCpscount() const {
		return _cpscount;
	}

	DmaxbScan& setCpscount(DMAXB_CpsCount cpscount) {
		_cpscount = cpscount;
		return *this;
	}

	DMAXB_LRSetting getLrsetting() const {
		return _lrsetting;
	}

	DmaxbScan& setLrsetting(DMAXB_LRSetting lrsetting) {
		_lrsetting = lrsetting;
		return *this;
	}

	DMAXB_ModeTwo getModetwo() const {
		return _modetwo;
	}

	DmaxbScan& setModetwo(DMAXB_ModeTwo modetwo) {
		_modetwo = modetwo;
		return *this;
	}

	char* getReadbuff() const {
		return _readbuff;
	}

	DMAXB_Scan getScan() const {
		return _scan;
	}

	DmaxbScan& setScan(DMAXB_Scan scan) {
		_scan = scan;
		return *this;
	}

	DMAXB_Unit getUnit() const {
		return _unit;
	}

	DmaxbScan& setUnit(DMAXB_Unit unit) {
		_unit = unit;
		return *this;
	}

	char* getWritebuff() const {
		return _writebuff;
	}

	int getEndAngle() const {
		return _endAngle;
	}

	DmaxbScan& setEndAngle(int endAngle) {
		_endAngle = endAngle;
		return *this;
	}

	float getFtTime() const {
		return _ftTime;
	}

	DmaxbScan& setFtTime(float ftTime) {
		_ftTime = ftTime;
		return *this;
	}

	int getModeThree() const {
		return _modeThree;
	}

	DmaxbScan& setModeThree(int modeThree) {
		_modeThree = modeThree;
		return *this;
	}

	float getSampleInterval() const {
		return _sampleInterval;
	}

	DmaxbScan& setSampleInterval(float sampleInterval) {
		_sampleInterval = sampleInterval;
		return *this;
	}

	float getScanSpeed() const {
		return _scanSpeed;
	}

	DmaxbScan& setScanSpeed(float scanSpeed) {
		_scanSpeed = scanSpeed;
		return *this;
	}

	int getStartAngle() const {
		return _startAngle;
	}

	DmaxbScan& setStartAngle(int startAngle) {
		_startAngle = startAngle;
		return *this;
	}

	long getFullScale() const {
		return _fullScale;
	}

	DmaxbScan& setFullScale(long fullScale) {
		_fullScale = fullScale;
		return *this;
	}

	// other function definitions.
	DmaxbScan& initialize(int comPort, int baudRate = 9600);
	DmaxbScan& prepareSendData();
	DmaxbScan& prepareSendDataHex();
	DmaxbScan& writeToSerial();
	DmaxbScan& readFromSerial();
	DmaxbScan& analyzeData();

	// helper function
	std::string _prepareSendDataString();

	void close();
	DmaxbScan& print();


};

#endif /* CDMAXBSCAN_H_ */
