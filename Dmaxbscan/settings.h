/*
 * settings.h
 *
 *  Enumerations for various settings related to DMAX-B settings.
 *
 *  Created on: Oct 29, 2012
 *      Author: Tharanga Mahakumarage
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

enum DMAXB_LRSetting {
	RIGHT = 1, LEFT = 2
};

enum DMAXB_Unit {
	GONIOMETER = 1, // Goniometer
	POLEFIGURE,      // Pole figure diffractometer attachment
	FIBERSPECIMEN      // Fiber specimen attachment.
};

enum DMAXB_Axis {
	TWOTHETATHETA = 1, // 20/0
	TWOTHETA,          // 20
	THETA              // 0
};

enum DMAXB_Scan {
	CONTINUOUS = 1, STEP
};

enum DMAXB_ModeTwo {
	REPEAT = 1, CUMULATIVE, SKIP
};

enum DMAXB_CpsCount {
	CPS_LINEAR = 1, COUNT_LINEAR, CPS_LOG, COUNT_LOG
};

#endif /* SETTINGS_H_ */
