#include <iostream>
#include <limits>
#include <iomanip>
#include "Serial.h"
#include "cDmaxscan.h"

using namespace std;

void _getAxis( int& axis ) {

    for(;;) {
        cout << "Please insert AXIS CODE ( 1: 2-theta theta | 2: 2-theta | 3: theta ) : ";

        if( cin >> axis ) {
            if( axis > 0 && axis < 4 ) {
                break;
            }
        }

        cin.clear();
        cin.ignore( numeric_limits<streamsize>::max(), '\n' );
        cout << "Please insert a valid choice" << endl;
    }

    return;
}


void _getStartAngle( float& startAngle ) {
    for(;;) {
        cout << "Please insert START ANGLE ( 2.0 - 145.0 ) : ";

        if( cin >> startAngle ) {
            if( startAngle >= 2 && startAngle <= 145 ) {
                break;
            }
        }


        cin.clear();
        cin.ignore( numeric_limits<streamsize>::max(), '\n' );
        cout << "Please insert a valid choice" << endl;
    }

    return;
}

void _getEndAngle( float& endAngle ) {
    for(;;) {
        cout << "Please insert END ANGLE ( 2.0 - 145.0 ) : ";

        if( cin >> endAngle ) {
            if( endAngle >= 2 && endAngle <= 145 ) {
                break;
            }
        }

        cin.clear();
        cin.ignore( numeric_limits<streamsize>::max(), '\n' );
        cout << "Please insert a valid choice" << endl;
    }

    return;
}

void _getTime( int& time ) {

    for(;;) {
        cout << "Please insert TIME ( 1 -   0.1 | 2 -   0.2 | 3 -   0.4 | 4 -     1 )" << endl;
        cout << "                   ( 5 -     2 | 6 -     4 | 7 -    10 | 8 -    20 )" << endl;
        cout << "                   ( 9 -    40 |10 -   100 |11 -   200 |12 -   400 )" << endl;
        cout << "                   (13 -  1000 |14 -  2000 |15 -  4000 |16 - 10000 ) : ";
        if( cin >> time ) {
            if( time > 0 && time < 17 ) {
                break;
            }
        }

        cin.clear();
        cin.ignore( numeric_limits<streamsize>::max(), '\n' );
        cout << "Please insert a valid choice" << endl;
    }

    return;
}


void _getStepWidth( int& stepWidth, bool theta ) {

    for(;;) {
        if( !theta ) {
            cout << "Please insert step Width ( 1: 0.002 | 2: 0.004 | 3 or 4: 0.01 | 5: 0.02 | 6: 0.05 | 7: 0.1 | 8: 0.2 | 9: 0.5 | 10: 1 | 11 : 2 ) : ";
        } else {
            cout << "Please insert step Width ( 1: 0.001 | 2: 0.002 | 3: 0.005 | 4: 0.01 | 5: 0.02 | 6: 0.05 | 7: 0.1 | 8: 0.2 | 9: 0.5 | 10 or 11: 1 ) : ";
        }

        if( cin >> stepWidth ) {
            if( stepWidth > 0 && stepWidth < 12 ) {
                break;
            }
        }

        cin.clear();
        cin.ignore( numeric_limits<streamsize>::max(), '\n' );
        cout << "Please insert a valid choice" << endl;

    }

    return;

}

float _calculateScanTime( cDmaxscan& serialObj ){
    float sAngle = serialObj.GetStartAngle();
    float eAngle = serialObj.GetEndAngle();
    float actStepWidth;
    float actTime;
    int axis = serialObj.GetAxis();
    bool theta = ( axis == 1 || axis == 2 ) ? FALSE : TRUE;

    switch( serialObj.GetStepWidth() ){
        case  1: actStepWidth = theta ? 0.001 : 0.002;
            break;
        case  2: actStepWidth = theta ? 0.002 : 0.004;
            break;
        case  3: actStepWidth = theta ? 0.005 : 0.01;
            break;
        case  4: actStepWidth = 0.01;
            break;
        case  5: actStepWidth = 0.02;
            break;
        case  6: actStepWidth = 0.05;
            break;
        case  7:  actStepWidth = 0.1;
            break;
        case  8:  actStepWidth = 0.2;
            break;
        case  9:  actStepWidth = 0.5;
            break;
        case 10:  actStepWidth = 1;
            break;
        case 11:   actStepWidth = theta ? 1 : 2;
            break;
        default: return 0;
    }

    switch( serialObj.GetTime() ){
        case   1: actTime = 0.1;
            break;
        case   2: actTime = 0.2;
            break;
        case   3: actTime = 0.4;
            break;
        case   4: actTime = 1;
            break;
        case   5: actTime = 2;
            break;
        case   6: actTime = 4;
            break;
        case   7: actTime = 10;
            break;
        case   8: actTime = 20;
            break;
        case   9: actTime = 40;
            break;
        case  10: actTime = 100;
            break;
        case  11: actTime = 200;
            break;
        case  12: actTime = 400;
            break;
        case  13: actTime = 1000;
            break;
        case  14: actTime = 2000;
            break;
        case  15: actTime = 4000;
            break;
        case  16: actTime = 10000;
            break;
        default:
            return 0;
    }

    return ( (1.99873*actStepWidth + 0.240195 + actTime ) * ( (eAngle - sAngle) / actStepWidth ) ) ;


}

void getInputs() {
    int axis;
    float startAngle;
    float endAngle;
    int stepWidth;
    int time;
    int totalTime;
    bool theta;

    cDmaxscan serialObj;

    _getAxis( axis );
    _getStartAngle( startAngle );
    _getEndAngle( endAngle );

    theta = ( axis == 1 || axis == 2 ) ? FALSE : TRUE;

    _getStepWidth( stepWidth, theta);
    _getTime( time );



//     //set axis value to cDmaxscan object
    serialObj.SetupSerial()
             .SetAxis( axis )
             .SetStartAngle( startAngle )
             .SetEndAngle( endAngle )
             .SetStepWidth( stepWidth )
             .SetTime( time );

    cout <<endl<< "Starting Scan" << endl;
    totalTime = _calculateScanTime( serialObj );
    cout << setprecision(3) << fixed << "Scan will take " << ( totalTime / 60 ) << " mins ( " << ( totalTime / 3600 ) << "hrs )"  ;
    cout <<endl<< "Sending Scan Data" << endl;

    serialObj.PrepareSendData();
    cout << "Sending command to COM3 [" << serialObj.GetOString() << "]" << endl;
    serialObj.SendToSerial();
    serialObj.ReadFromSerial().Print().CloseSerial();
    cout << "Received from COM3 [" << serialObj.GetIString() << "]" << endl;
    return;
}


int main() {
    // bool exitFlag = FALSE;
    char input = 'N';
    bool firstRun = TRUE;

    while( firstRun || input == 'y' || input == 'Y' ) {
        getInputs();

        if( firstRun ) {
            firstRun = FALSE;
        }

        cout << "Do you want to continue ( Y or y ) : ";
        cin >> input;
    }

}
