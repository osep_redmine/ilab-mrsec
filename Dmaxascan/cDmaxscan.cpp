#include "cDmaxscan.h"
#include <iomanip>
#include <sstream>
#include <iostream>
#include <cstring>

using namespace std;

cDmaxscan::cDmaxscan() {
	oSerial = new CSerial;
	memset(iString, 0, MAX_READ_BUFF_SIZE);
}

cDmaxscan::~cDmaxscan() {
	delete oSerial;
}

// this function prepares the string so that it can be transmitted to serial port

cDmaxscan& cDmaxscan::PrepareSendData() {
	stringstream ss;

	// setting up the output string.
	ss << "15 " << setfill('0') << fixed << right << setprecision(0) << setw(2)
			<< iAxis << " " << setw(8) << iStartAngle * 1000 << " " << setw(8)
			<< iEndAngle * 1000 << " " << setw(2) << iStepWidth << " " << "16 "
			<< setw(2) << iTime << " " << "2";

	strcpy(oString, ss.str().c_str());
	return *this;
}

cDmaxscan& cDmaxscan::SetupSerial() {
	oSerial->Open(3, 9600);
	return *this;
}

cDmaxscan& cDmaxscan::SendToSerial() {
	oSerial->SendData(oString, strlen(oString));
	return *this;
}

cDmaxscan& cDmaxscan::ReadFromSerial() {
	// if( oSerial->ReadDataWaiting()){
	int ret, i;

	cout << "Waiting for data ";

	for (i = 0; i < 25; i++) {
		if (oSerial->ReadDataWaiting()) {
			break;
		}

		cout << ".";
		Sleep(500);
	}

	cout << endl << "Successfully read data from Serial Port" << endl;

	ret = oSerial->ReadData((void *) iString, MAX_READ_BUFF_SIZE);
	strncpy(iString + ret, "\0", ret);
	return *this;
}

void cDmaxscan::CloseSerial() {
	oSerial->Close();
	cout << "Serial Port Closed" << endl;
}

cDmaxscan& cDmaxscan::Print() {
	int i;
	int len = strlen(iString);
	cout.flush();
	for (i = 0; i < len; i++)
		cout << hex << (int) iString[i] << " ";
	cout << endl;

	for (i = 0; i < len; i++)
		cout << iString[i] << "  ";
	cout << endl;

	return *this;

}
