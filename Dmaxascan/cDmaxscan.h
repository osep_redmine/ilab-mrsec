#ifndef CDMAXSCAN_H
#define CDMAXSCAN_H

#include <string>
#include "Serial.h"

using namespace std;

#define MAX_READ_BUFF_SIZE 50
#define MAX_WRITE_BUFF_SIZE 35

class cDmaxscan
{
    public:
        cDmaxscan();

        virtual ~cDmaxscan();

        int GetAxis() { return iAxis; }

        cDmaxscan &SetAxis(int val) { iAxis = val; return *this; }

        float GetStartAngle() { return iStartAngle; }

        cDmaxscan &SetStartAngle(float val) { iStartAngle = val; return *this; }

        float GetEndAngle() { return iEndAngle; }

        cDmaxscan &SetEndAngle(float val) { iEndAngle = val; return *this; }

        int GetStepWidth() { return iStepWidth; }

        cDmaxscan &SetStepWidth(int val) { iStepWidth = val; return *this; }

        int GetTime() { return iTime; }

        cDmaxscan &SetTime(int val) { iTime = val; return *this; }

        string GetTwoTheta() { return oTwoTheta; }

        cDmaxscan &SetTwoTheta(string val) { oTwoTheta = val; return *this; }

        string GetAngle() { return oAngle; }

        cDmaxscan &SetAngle(string val) { oAngle = val; return *this; }

        char* GetOString(){return oString;}

        char* GetIString(){return iString;}

        cDmaxscan &PrepareSendData();

        cDmaxscan &SetupSerial();
        cDmaxscan &SendToSerial();
        cDmaxscan &ReadFromSerial();
        cDmaxscan &Print();
        void CloseSerial();


    protected:
    private:
        int iAxis;
        float iStartAngle;
        float iEndAngle;
        int iStepWidth;
        int iTime;
        string oTwoTheta;
        string oAngle;
        char oString[MAX_WRITE_BUFF_SIZE];
        char iString[MAX_READ_BUFF_SIZE];
        CSerial* oSerial;
};

#endif // CDMAXSCAN_H
